import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="demo_predict",
    version="0.0.1",
    author="Max Polaczuk",
    author_email="maxpolaczuk@gmail.com",
    description="Package to interface with ",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://sportsflare.io",
    packages=['demo_predict'],
    package_dir={'':'src'},
    install_requires=['numpy', 'pandas'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
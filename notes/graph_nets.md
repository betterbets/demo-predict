# Graph Neural Networks

## Event driven round prediction

This can be approached in a few different ways. 

- Edge level prediction
    - Given the most recent action and the previous state(s) of the network for a round,
    predict the round winner
- Player level prediction
    - Given events between nodes (as above), add player information to the model. 
- Movement level prediction
    - Given player 
   
Useful outcomes of event-driven prediction:

- Auto Highlighting
- Live Betting odds calculation
- Overlay for pro games

Useful outcomes of movement-driven predictions:
- Coaching
- Move suggestion

## Imitation Learning of Players

Use adversarial imitation learning to learn the behaviour of specific players.
Create smart and stylized bots.

Useful outcomes include:

- Coaching
- Training

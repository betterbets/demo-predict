FROM continuumio/miniconda3

# TODO : Get the data file from S3
# For now just add the data file in with the code
ADD . /demopredict
WORKDIR demopredict

RUN conda env create -f environment.yml

# Pull the environment name out of the environment.yml to automatically activate env
RUN echo "source activate $(head -1 environment.yml | cut -d' ' -f2)" > ~/.bashrc
ENV PATH /opt/conda/envs/$(head -1 environment.yml | cut -d' ' -f2)/bin:$PATH

# Install pytorch
RUN conda install pytorch torchvision -c pytorch

# Register all conda environments as kernels in jupyter
RUN conda install nb_conda

# Install demo-predict
RUN pip install -e .

# Start jupyter notebook server for working in
CMD jupyter notebook --port=1234 --no-browser --ip=0.0.0.0 --allow-root
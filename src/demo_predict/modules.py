
import torch
import torch.nn as nn


class RoundNet(nn.Module):
    def __init__(self, hidden_size=256, num_players=10):
        """Pytorch module for RoundNet.

        RoundNet predicts the winner of a round at each event in the game, using a graph data structure.

        Version 1:
            There is no auxiliary player information, and the model is event driven.
        Version 2:
            Introduce auxiliary player embeddings - with model still event driven.
        Version 3:
            Discrete timesteps between events introduced.

        :param hidden_size: Size of the hidden state.
        :param num_players: The number of players in a CS:GO game.
        """
        super(RoundNet, self).__init__()

        self.global_to_hidden_1 = nn.Linear(4, int(hidden_size / 2))
        self.global_to_hidden_2 = nn.Linear(int(hidden_size / 2), hidden_size)

        self.node_attributes_1 = nn.Linear(num_players * 2, hidden_size)
        self.node_attributes_2 = nn.Linear(hidden_size, hidden_size)

        # Edges are of shape (N, T, num_players * 2)
        self.edge_sequence = nn.GRU(num_players * 2 + 1, hidden_size, batch_first=True)

        self.final_1 = nn.Linear(hidden_size, hidden_size * 2)
        self.final_2 = nn.Linear(1, hidden_size)

    def forward(self, input_globals, input_node_attr, input_edge_seq):
        # Project globals to hidden state
        h = self.global_to_hidden_1(input_globals)
        h = nn.ELU()(h)
        h = self.global_to_hidden_2(h)
        h = nn.ELU()(h)

        # Run through RNN of edge sequence, using projection (h) as intial hidden state
        _, edge_seq = self.edge_sequence(input_edge_seq, h)

        # Get node attribute representation as initial hidden state (project up to hidden dimension)
        node_rep = self.node_attributes_1(input_node_attr)
        node_rep = nn.ELU()(node_rep)
        node_rep = self.node_attributes_2(node_rep)
        node_rep = nn.ELU()(node_rep)

        # Append node attributes and the RNN final hidden state
        x = torch.cat([edge_seq, node_rep], dim=1)

        x = self.final_1(x)
        x = nn.ELU()(x)
        x = self.final_2(x)
        y = nn.Sigmoid()(x)

        return y


class KillNet(nn.Module):
    def __init__(self, hidden_size=256, num_players=10):
        """Pytorch module for KillNet.

        KillNet predicts the next player to kill and be killed, using a graph data structure.
        KillNet provides a multinomial output which can easily be converted to odds.

        Version 1:
            At each event predict the odds of who will get the next the next kill.
        Version 2:
            At each event predict the odds of who will get the next kill, and who will be the victim.
        Version 3:
            Based on positions predict the odds of who will kill and be killed.

        :param hidden_size: Size of the hidden state.
        :param num_players: The number of players in a CS:GO game.
        """
        super(KillNet, self).__init__()

        self.global_to_hidden_1 = nn.Linear(4, int(hidden_size / 2))
        self.global_to_hidden_2 = nn.Linear(int(hidden_size / 2), hidden_size)

        self.node_attributes_1 = nn.Linear(num_players * 2, hidden_size)
        self.node_attributes_2 = nn.Linear(hidden_size, hidden_size)

        # Edges are of shape (N, T, num_players * 2)
        self.edge_sequence = nn.GRU(num_players * 2 + 1, hidden_size, batch_first=True)

        self.final_1 = nn.Linear(hidden_size, hidden_size * 2)
        self.kill_dist = nn.Linear(hidden_size, num_players)
        self.damage = nn.Linear(hidden_size, 1)

    def forward(self, input_globals, input_node_attr, input_edge_seq):
        # Project globals to hidden state
        h = self.global_to_hidden_1(input_globals)
        h = nn.ELU()(h)
        h = self.global_to_hidden_2(h)
        h = nn.ELU()(h)

        # Run through RNN of edge sequence, using projection (h) as intial hidden state
        _, edge_seq = self.edge_sequence(input_edge_seq, h)

        # Get node attribute representation as initial hidden state (project up to hidden dimension)
        node_rep = self.node_attributes_1(input_node_attr)
        node_rep = nn.ELU()(node_rep)
        node_rep = self.node_attributes_2(node_rep)
        node_rep = nn.ELU()(node_rep)

        # Append node attributes and the RNN final hidden state
        x = torch.cat([edge_seq, node_rep], dim=1)

        x = self.final_1(x)
        x = nn.ELU()(x)
        x = self.kill_dist(x)
        y = nn.LogSoftmax(dim=1)(x)

        return y


class EdgeNet(nn.Module):
    def __init__(self, hidden_size=256, num_players=10):
        """Pytorch module for EdgeNet.

        EdgeNet provides a prediction of the next edge.
        It can be used to create dynamic odds for the next event to occur.

        Version 1:
            At each event predict the next event: | ATTACKER | VICTIM | DAMAGE |

        :param hidden_size: Size of the hidden state.
        :param num_players: The number of players in a CS:GO game.
        """
        super(EdgeNet, self).__init__()

        self.global_to_hidden_1 = nn.Linear(4, int(hidden_size / 2))
        self.global_to_hidden_2 = nn.Linear(int(hidden_size / 2), hidden_size)

        self.node_attributes_1 = nn.Linear(num_players * 2, hidden_size)
        self.node_attributes_2 = nn.Linear(hidden_size, hidden_size)

        # Edges are of shape (N, T, num_players * 2 + 1), the +1 corresponds to damage
        self.edge_sequence = nn.GRU(num_players * 2 + 1, hidden_size, batch_first=True)

        self.final_1 = nn.Linear(hidden_size, hidden_size * 2)
        self.victim_dist = nn.Linear(num_players, hidden_size)
        self.att_dist = nn.Linear(num_players, hidden_size)

    def forward(self, input_globals, input_node_attr, input_edge_seq):
        # Project globals to hidden state
        h = self.global_to_hidden_1(input_globals)
        h = nn.ELU()(h)
        h = self.global_to_hidden_2(h)
        h = nn.ELU()(h)

        # Run through RNN of edge sequence, using projection (h) as intial hidden state
        _, edge_seq = self.edge_sequence(input_edge_seq, h)

        # Get node attribute representation as initial hidden state (project up to hidden dimension)
        node_rep = self.node_attributes_1(input_node_attr)
        node_rep = nn.ELU()(node_rep)
        node_rep = self.node_attributes_2(node_rep)
        node_rep = nn.ELU()(node_rep)

        # Append node attributes and the RNN final hidden state
        x = torch.cat([edge_seq, node_rep], dim=1)

        x = self.final_1(x)
        x = nn.ELU()(x)
        x = self.kill_dist(x)
        y = nn.LogSoftmax(dim=1)(x)

        return y


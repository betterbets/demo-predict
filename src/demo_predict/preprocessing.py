
import torch

import numpy as np
import pandas as pd


class DemoDataFile(object):
    def __init__(self, input_name=None):
        """"""
        if not input_name:
            input_name = 'mm_master_demos.csv'
        # Load data
        self.df = pd.read_csv(input_name)


def build_round_graph(df, game_id, round_number=1):
    """Builds the game network for one specific round from datafile of type `1`.

    The game network is time-distributed, by the number of events in one round of CS:GO.
    It builds a Graph consisting of players (V), connections/events (E), and global (G) attributes about the
    environment.

    :param df: Pandas dataframe of datafile type 1.
    :param game_id: String of the demo file id.
    :param round_number: Which round of the demo to build a temporal graph for.
    """

    # Subset data
    round_df = df[(df['round'] == round_number) & (df.file == game_id)].reset_index(drop=True)
    n_events = len(round_df)

    # Initialize time-dependent information
    V = []
    E = []
    GLOBALS = [
        {
            'round': round_number,
            't_money': round_df['t_eq_val'].iloc[0],
            'ct_money': round_df['ct_eq_val'].iloc[0],
            'bomb_planted': 0.,
            'terrorist_win': 1. if round_df['winner_side'].iloc[0] == 'Terrorist' else 0.,
        }
    ]

    # Initialize nodes (vertices) : assign first 5 players of each team as a node
    nodes = {}

    terr_att = round_df[round_df['att_side'] == 'Terrorist'].att_id.unique()
    terr_vic = round_df[round_df['vic_side'] == 'Terrorist'].vic_id.unique()
    terr_id = np.unique(np.append(terr_att, terr_vic))
    tmp_node = {id_: {'node_id': i, 'team': 'Terrorist', 'health': 100.} for i, id_ in enumerate(terr_id)}
    if len(tmp_node) < 5:
        num_to_pad = 5 - len(tmp_node)
        for i in range(num_to_pad):
            tmp_node[f'PAD_{i+1}'] = {'node_id': len(tmp_node) + i, 'team': 'Terrorist', 'health': 0.}
    nodes.update(tmp_node)

    ct_att = round_df[round_df['att_side'] == 'CounterTerrorist'].att_id.unique()
    ct_vic = round_df[round_df['vic_side'] == 'CounterTerrorist'].vic_id.unique()
    ct_id = np.unique(np.append(ct_att, ct_vic))
    tmp_node = {id_: {'node_id': i, 'team': 'CounterTerrorist', 'health': 100.} for i, id_ in enumerate(ct_id)}
    if len(tmp_node) < 5:
        num_to_pad = 5 - len(tmp_node)
        for i in range(num_to_pad):
            tmp_node[f'PAD_{i+1}'] = {'node_id': len(tmp_node) + i, 'team': 'CounterTerrorist', 'health': 0.}
    nodes.update(tmp_node)

    # Initial vertices : note that there are no edges yet
    V.append(nodes)

    # Build edges at each step
    for event in range(n_events):
        new_nodes = {key: item.copy() for key, item in V[event].items()}  # Was overwriting itself

        # Add edge between sender and receiver
        new_edge = {
                "receiver": round_df['att_id'].iloc[event],
                "sender": round_df['vic_id'].iloc[event],
                "weapon_type": round_df['wp_type'].iloc[event],
                "damage": round_df['hp_dmg'].iloc[event]
                }
        if event == 0:
            # First edge to log
            E.append([new_edge])
        else:
            # Add also last edges
            E.append(E[event-1] + [new_edge])

        # Update nodes with "damage" --
        new_nodes[new_edge['sender']]['health'] = new_nodes[new_edge['sender']]['health'] - new_edge['damage']
        V.append(new_nodes.copy())

        # Update globals
        new_global = GLOBALS[event].copy()
        if round_df['is_bomb_planted'].iloc[event]:
            new_global['bomb_planted'] = 1.

        GLOBALS.append(new_global)

    return V, E, GLOBALS


# At each event in a round we begin with a graph = Gt(Vt, Et, At)
"""
1.) Compute `NEXT_EDGE` predictions - with `edge_network` 
    Given "NULL" edges of a fully connected graph (with multi connections).
    For each combination of nodes choose whether to "ADD" an edge and what
    the predicted edge looks like.

    For our first task we can predict a `sender`, `receiver`, `damage`
    `NEXT_EDGE`, as we update the graph by one event at each point.

    Compute cross entropy loss, for sender & receiver & MSE for damage.

    Add most likely edge to the graph as a `NEW_EDGE`.

2.) Aggregate edges per node - using a simple aggregation function (i.e. sum)
    Compute aggregation over all inbound edges to that node.
    Basically this means that players who have done lots of damage will have
    a lot of inbound connections.

3.) Update node status - with hard coding
    Given the predicted new world state, update the node status for each 
    node in the graph. 

    However for our use case (at this stage) - 
    there are no predictive node attributes. So we just update the 
    predicted damage as (`health` - `new_damage`).

4.) Update globals - using `global_network`
    For now, we just want to predict the "T_WIN_PROBABILITY"
    given the input graph.

    Inputs:
        - Prepopulate the hidden state for edges with projection network
            - Map
            - CT_economy
            - T_economy
            - Bomb_planted
        - Node attributes - (first 5 positions are T, second 5 are CT)
            - Healths [100, 100, 25, 20, 0, ... ] (d=10)
            - Damage given = sum(inbound_edge_damage_dealt) [300, 0, 25, ... ] (d=10)
        - Sequence of edges leading up to time `t` (N, t, d=21)
            - Sender [0, 0, 1, 0, ..., 0] (d=10)
            - Receiver [0, 0, 0, 1, ..., 0] (d=10)
            - Damage [25] (d=1)
    Outputs:
        - P(Terrorist_Round_Winner | Gt, Gt-1, ..., G0)

# We can ignore `NEXT_EDGE` prediction for now -- just focus on round winner
"""


def input_data_from_round_graph(event_num, vertices, edges, globals):
    """Create input data tensors, and target at each event from a graph.

    :param event_num: The number of the event which to make cumulative tensors at.
    :param vertices: The list of vertices at each point of the demo.
    :param edges: The list of dictionaries containing cumulative event information.
    :param globals: The global variables at each event-step. These include map, round,
                    bomb planted, economy and ticker time.
    :return: Tuple containing `input_globals`, `input_node_attr`, `input_edge_seq`, `target` tensors.
    """

    # Create a player lineup per team:
    terr = []
    ct = []
    t_health = []
    ct_health = []
    for node, data in vertices[event_num].items():
        if data['team'] == 'Terrorist':
            terr.append(node)
            t_health.append(data['health'])
        else:
            ct.append(node)
            ct_health.append(data['health'])

    # Lineup of players
    players = terr + ct
    player_lookup = {p: i for i, p in enumerate(players)}
    healths = torch.FloatTensor(t_health + ct_health) / 100.

    # Summation of each inbound node -- of shape (receiver, sender)
    node_scores = torch.zeros(len(players), len(players)) / 100.

    # Make input sequence of edges
    input_edge_seq = []
    for edge in edges[event_num]:
        # Lookup sender ID & onehot
        sender = torch.zeros(len(players))
        sender[player_lookup[edge['sender']]] = 1.

        # Lookup receiver ID
        receiver = torch.zeros(len(players))
        receiver[player_lookup[edge['receiver']]] = 1.

        # Damage attribute type -- normalize to be a percentage
        dmg = torch.FloatTensor([edge['damage'] / 100])

        # Update the node summation: receiver gets the damage
        send_idx = player_lookup[edge['sender']]
        receive_idx = player_lookup[edge['receiver']]
        node_scores[receive_idx, send_idx] += edge['damage'] / 100

        # Concatenate these features together
        edge_inp = torch.cat([sender, receiver, dmg]).unsqueeze(0)
        input_edge_seq.append(edge_inp)

    # Make torch arrays
    input_edge_seq = torch.cat(input_edge_seq)
    node_scores = node_scores.sum(dim=1)
    input_node_attr = torch.cat([healths, node_scores])
    globs = globals[event_num]
    input_globals = torch.FloatTensor([
        globs['ct_money'] / 10000.,
        globs['t_money'] / 10000.,
        globs['bomb_planted'],
        globs['round']/16.
    ])

    target = torch.FloatTensor([globs['terrorist_win']])

    return input_globals, input_node_attr, input_edge_seq, target

import json
import pandas as pd


FILENAME = 'demo.json'


class ParsedDemo(object):
    """Use CSMinifier to build a game.

    CSMinifier is bad at capturing valuable meta information of the game.
    Ideally this is just a test and we will change to a better quality system.

    We build the game engine below from a parsed demo file, and handle all of the preprocessing for a
    ML algorithm to consume.
    """

    def __init__(self, filepath=FILENAME):
        self.filepath = filepath
        self.data = None
        self.snapshot_df = None
        self.ticklist = None
        self.rounds = None
        self.team_a = None
        self.team_b = None

        self.player_id_to_idx = {}

        # Build data from self
        self._load_file()

    def _load_file(self, filepath=None):
        """Load in the parsed CS:GO demo file."""
        if filepath:
            self.filepath = filepath

        with open(self.file, 'r') as f:
            self.data = json.load(f)

        # Create useful data assets
        self.snapshot_df = pd.DataFrame(self.data['snapshots'])
        self.ticklist = self.data['ticks']

    def build_rounds_from_data(self):
        """"""
        # Start counter from first "round_started" event
        # Find each rounds "closing point" tick (T)
        # Take snapshot closest as tick approaches T from the left
        # (For now) call team with most HP as the winner

        init = True
        rounds = []

        for event in self.ticklist:

            tick = event['nr']

            if init and any('round_started' in e for e in event['events']):
                # Game starts here
                rounds.append(self.initialize_round_dict(len(rounds), tick))
                init = False

            elif any('round_started' in e for e in event['events']):
                # New round - finalize the previous round's details
                last_round = len(rounds)
                last_tick = self.closest_index(self.snapshot_df['tick'], tick)

                # Get remaining team healths
                a_health, b_health = self.get_healths_at_snapshot(last_tick)

                if a_health > b_health:
                    winner = 'team_a'
                elif b_health > a_health:
                    winner = 'team_b'
                else:
                    winner = 'draw'

                # Add attributes
                rounds[last_round]['team_a_health'] = a_health
                rounds[last_round]['team_b_health'] = b_health
                rounds[last_round]['winner'] = winner

                # Add new point to the data
                rounds.append(self.initialize_round_dict(len(rounds), tick))

        return rounds

    @staticmethod
    def initialize_round_dict(round_number, start_tick):
        """"""

        round_dict = {
            "round": round_number,
            "start_tick": start_tick,
            "end_tick": None,
            "team_a_health": None,
            "team_b_health": None,
            "winner": None,
        }

        return round_dict

    def build_player_lookup_from_data(self):
        """"""

        player_name_from_id = {}

        # Store for team "2" and "3"
        for player in self.data['entities']:
            if player['team'] in (2, 3):
                if player['team'] == 2:
                    self.team_a.append(player['id'])
                elif player['team'] == 3:
                    self.team_b.append(player['id'])
                # Add player
                player_name_from_id[player['id']] = player['name']

        # Construct a lookup and reverse lookup
        self.player_id_to_idx = {player_id: i for i, player_id in enumerate(player_name_from_id)}

    @staticmethod
    def closest_index(indices, query, how='left'):
        """Find closest number in list to query."""
        if how == 'left':
            indices = indices[indices <= query]
            return indices.max()
        else:
            raise NotImplementedError("Cannot find closest from right yet.")

    def get_healths_at_snapshot(self, tick_id):
        """Retrieve each teams remaining health at a given time."""
        entities = self.snapshot_df.entityUpdates.iloc[tick_id]

        # Iterate over each team's entities
        # Sum the health of each team
        team_a_health = 0
        team_b_health = 0

        for entity in entities:
            # Add health to the correct team
            if entity['entityId'] in self.team_a:
                team_a_health += entity['hp']
            elif entity['entityId'] in self.team_b:
                team_b_health += entity['hp']

        return team_a_health, team_b_health




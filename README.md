# CS:GO Demo Predict

Predict in-game attributes using graph neural networks on CS:GO demo files.

We apply cutting edge research to the task of understanding the game of CS:GO.

This repository provides you with a consistent research environment to work on
CS:GO demo predictions.

## Getting Started

To get started just type:
```bash
docker-compose build
```

Then run:
```bash
docker-compose up
```

You will have a Jupyter notebook running at `localhost:1234` with access to the 



## Demo Data 1

Our prototype dataset consists of attributes and events from match-making (MM) and ESEA data
collected over 2 weeks on Kaggle.

We build a graph neural network model to predict the round winner in real time,
and explore the learned network weights in order to deliver a never-before-seen
prototype to market.

The goal of this project is to gain kudos from the machine learning community,
gather attention from media companies (ESL, Twitch), gaming companies (VALVE) 
and ultimately investors (to help us build the full version). 

The late game is that we can apply similar techniques to multiple esports.

The data file can be downloaded from here:

```bash
s3://some_path_here
```

